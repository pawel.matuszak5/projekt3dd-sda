using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SDA.UI;

namespace SDA.Loop
{
    public class MainMenuState : IBaseState

    {

        private Action transitionToGameState;
        private MainMenuView mainMenuView;

        public MainMenuState(Action transitionToGameState, MainMenuView mainMenuView)
            {

            this.transitionToGameState = transitionToGameState;
            this.mainMenuView = mainMenuView;


            }

         public void InitState()
            {
                mainMenuView.ShowView();
            }

         public void UpdateState()
            {
                Debug.Log("Dupa");




            }

         public void DisposeState()
            {
                mainMenuView.HideView();
            }

  }
}
