using System;
using UnityEngine;
using SDA.UI;
using SDA.Input;



namespace SDA.Loop
{

    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private MainMenuView mainMenuView;

        [SerializeField]
        private StackInput stackInput;


        private MainMenuState mainMenuState;
        private GameState gameState;

        private Action TransitionToGameState;

        private IBaseState currentlyActiveState;

        private void Start()
        {

            TransitionToGameState += () => ChangeState(gameState);

            mainMenuState = new MainMenuState(TransitionToGameState, mainMenuView);
            gameState = new GameState();


            

            ChangeState(mainMenuState);
        }

        private void Update()
        {
            currentlyActiveState?.UpdateState();
        }

        private void OnDestroy()
        {

        }

        private void ChangeState(IBaseState newState)
        {
            currentlyActiveState?.DisposeState();
            currentlyActiveState = newState;
            currentlyActiveState?.InitState();
        }
    }
}


